﻿using UnityEngine;

public class TitleGameController : MonoBehaviour
{
    private ScrollingBackground background;

    private Animator p1Anim, p2Anim, p3Anim, p4Anim;
    private bool p1Start, p2Start, p3Start, p4Start;

    private bool timerShow, timerShown;
    private GlobalInfo global;

    void Start()
    {
        global = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalInfo>();
        background = GetComponent<ScrollingBackground>();
        background.SetSpeed(20);
        GameObject.FindGameObjectWithTag("Player1").GetComponent<JimmyBehavior>().SetPlayerID(1);
        GameObject.FindGameObjectWithTag("Player2").GetComponent<JimmyBehavior>().SetPlayerID(2);
        GameObject.FindGameObjectWithTag("Player3").GetComponent<JimmyBehavior>().SetPlayerID(3);
        GameObject.FindGameObjectWithTag("Player4").GetComponent<JimmyBehavior>().SetPlayerID(4);
        GameObject.FindGameObjectWithTag("Player1").GetComponent<SpriteRenderer>().enabled = false;
        GameObject.FindGameObjectWithTag("Player2").GetComponent<SpriteRenderer>().enabled = false;
        GameObject.FindGameObjectWithTag("Player3").GetComponent<SpriteRenderer>().enabled = false;
        GameObject.FindGameObjectWithTag("Player4").GetComponent<SpriteRenderer>().enabled = false;
    }

    void Update()
    {
        if (!p1Start && Input.GetButtonDown("P1_Button1"))
        {
            p1Start = true;
            timerShow = true;
            global.Player1Ready = true;
            GameObject.FindGameObjectWithTag("Player1").GetComponent<SpriteRenderer>().enabled = true;
            GameObject.Find("Ready1Text").GetComponent<UILabel>().enabled = true;
            GameObject.Find("Ready1").GetComponent<UILabel>().enabled = false;
        }
        if (!p2Start && Input.GetButtonDown("P2_Button1"))
        {
            p2Start = true;
            timerShow = true;
            global.Player2Ready = true;
            GameObject.FindGameObjectWithTag("Player2").GetComponent<SpriteRenderer>().enabled = true;
            GameObject.Find("Ready2Text").GetComponent<UILabel>().enabled = true;
            GameObject.Find("Ready2").GetComponent<UILabel>().enabled = false;
        }
        if (!p3Start && Input.GetButtonDown("P3_Button1"))
        {
            p3Start = true;
            timerShow = true;
            global.Player3Ready = true;
            GameObject.FindGameObjectWithTag("Player3").GetComponent<SpriteRenderer>().enabled = true;
            GameObject.Find("Ready3Text").GetComponent<UILabel>().enabled = true;
            GameObject.Find("Ready3").GetComponent<UILabel>().enabled = false;
        }
        if (!p4Start && Input.GetButtonDown("P4_Button1"))
        {
            p4Start = true;
            timerShow = true;
            global.Player4Ready = true;
            GameObject.FindGameObjectWithTag("Player4").GetComponent<SpriteRenderer>().enabled = true;
            GameObject.Find("Ready4Text").GetComponent<UILabel>().enabled = true;
            GameObject.Find("Ready4").GetComponent<UILabel>().enabled = false;
        }

        if (timerShow && !timerShown)
        {
            timerShown = true;
            GetComponent<TimerStart>().StartTimer();
        }
    }

    public void StartGame()
    {
        Application.LoadLevel(1);
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }
}
