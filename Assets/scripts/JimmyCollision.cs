﻿using UnityEngine;
using System.Collections;

public class JimmyCollision : MonoBehaviour
{
    private Animator anim;
    private bool isDead;

    void Start()
    {
        anim = GetComponent<Animator>();
    }
	
    void OnTriggerEnter(Collider col)
    {
        if (!isDead && col.CompareTag("Hazard")) {
            AudioController.Play("trip");
            anim.SetTrigger("Fall");
            isDead = true;
        }

        if (!isDead && col.CompareTag("LeftBoundary"))
        {
            GetComponent<JimmyMovement>().canMoveLeft = false;
        }
        else if (!isDead && col.CompareTag("RightBoundary"))
        {
            GetComponent<JimmyMovement>().canMoveRight = false;
        }
    }
    
    public void PlayLandingSound()
    {
        AudioController.Play("landing");
    }
}
