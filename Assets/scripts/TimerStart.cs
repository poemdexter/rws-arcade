﻿using UnityEngine;
using System.Collections;

public class TimerStart : MonoBehaviour
{
    public int waitTime;
    private float timeLeft, currentTime;
    private bool timerRun;
    UILabel timeText;

    void Start()
    {
        timeText = GameObject.Find("Timer").GetComponent<UILabel>();
    }

    void Update()
    {
        if (timerRun)
        {
            timeLeft -= Time.deltaTime;
            waitTime = Mathf.CeilToInt(timeLeft);
            timeText.text = waitTime.ToString();

            if (timeLeft < 0)
            {
                GetComponent<TitleGameController>().StartGame();
            }
        }
    }

    public void StartTimer()
    {
        timeText.enabled = true;
        timerRun = true;
        timeLeft = waitTime;
    }
}
