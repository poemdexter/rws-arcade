﻿using UnityEngine;
using System.Collections;

public class GlobalInfo : MonoBehaviour
{
    public bool Player1Ready { get; set; }
    public bool Player2Ready { get; set; }
    public bool Player3Ready { get; set; }
    public bool Player4Ready { get; set; }

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
}
