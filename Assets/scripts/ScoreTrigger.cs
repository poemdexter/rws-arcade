﻿using System.Globalization;
using UnityEngine;
using System.Collections;

public class ScoreTrigger : MonoBehaviour
{
    public int score { get; private set; }

    SpawnHazards spawner;
    bool canScore = true;
    
    void Start()
    {
        spawner = GameObject.FindGameObjectWithTag("GameController").GetComponent<SpawnHazards>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (canScore) {
            score++;
            spawner.IncrementScore();

        }
    }

    public void OnDeath()
    {
        canScore = false;
    }
    
    public void OnReset()
    {
        canScore = true;
        score = 0;
    }
}
