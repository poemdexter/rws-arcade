﻿using UnityEngine;
using System.Collections;

public class JimmyMovement : MonoBehaviour
{
    public float speed;
    public float topBounds;
    public float bottomBounds;

    public bool canMoveLeft { get; set; }
    public bool canMoveRight { get; set; }

    void Start()
    {
        canMoveLeft = true;
        canMoveRight = true;
    }

    void Update()
    {
        Vector2 moveDirection = GetPlayerInput();
        if (moveDirection != Vector2.zero)
        {
            // up down
            float newZ = (moveDirection.y * speed * Time.deltaTime);
            newZ = (IsInBounds(newZ)) ? newZ : 0;

            // left right
            float newX = 0;
            newX = moveDirection.x * speed * Time.deltaTime;

            transform.localPosition += new Vector3(newX, 0, newZ);
        }
    }

    public void DeathAnimationFinished()
    {
        // TODO: start slowdown
    }

    private bool IsInBounds(float z)
    {
        return (z + transform.position.z < topBounds && z + transform.position.z > bottomBounds);
    }

    private Vector2 GetPlayerInput()
    {
        Vector2 ret = Vector2.zero;

        switch (gameObject.tag)
        {
            case "Player1":
                if (Input.GetButton("P1_Up"))
                    ret.y = 1f;
                else if (Input.GetButton("P1_Down"))
                    ret.y = -1f;
                if (canMoveRight && Input.GetButton("P1_Right"))
                {
                    canMoveLeft = true;
                    ret.x = 1f;
                }
                else if (canMoveLeft && Input.GetButton("P1_Left"))
                {
                    canMoveRight = true;
                    ret.x = -1f;
                }
                break;
            case "Player2":
                if (Input.GetButton("P2_Up"))
                    ret.y = 1f;
                else if (Input.GetButton("P2_Down"))
                    ret.y = -1f;
                if (canMoveRight && Input.GetButton("P2_Right"))
                {
                    canMoveLeft = true;
                    ret.x = 1f;
                }
                else if (canMoveLeft && Input.GetButton("P2_Left"))
                {
                    canMoveRight = true;
                    ret.x = -1f;
                }
                break;
            case "Player3":
                if (Input.GetButton("P3_Up"))
                    ret.y = 1f;
                else if (Input.GetButton("P3_Down"))
                    ret.y = -1f;
                if (canMoveRight && Input.GetButton("P3_Right"))
                {
                    canMoveLeft = true;
                    ret.x = 1f;
                }
                else if (canMoveLeft && Input.GetButton("P3_Left"))
                {
                    canMoveRight = true;
                    ret.x = -1f;
                }   
                break;
            case "Player4":
                if (Input.GetButton("P4_Up"))
                    ret.y = 1f;
                else if (Input.GetButton("P4_Down"))
                    ret.y = -1f;
                if (canMoveRight && Input.GetButton("P4_Right"))
                {
                    canMoveLeft = true;
                    ret.x = 1f;
                }
                else if (canMoveLeft && Input.GetButton("P4_Left"))
                {
                    canMoveRight = true;
                    ret.x = -1f;
                }
                break;
            default:
                return Vector2.zero;
        }
        return ret;
    }
}
