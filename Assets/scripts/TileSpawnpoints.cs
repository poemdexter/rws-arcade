﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileSpawnpoints : MonoBehaviour
{
    private List<Transform> hazardSpawns;
    
    void Start()
    {
        hazardSpawns = new List<Transform>();
        foreach (Transform child in transform) {
            if (child.CompareTag("hazardSpawnpoint")) {
                hazardSpawns.Add(child);
            }
        }
    }
    
    public Transform GetRandomHazardSpawnpoint()
    {
        return hazardSpawns[Random.Range(0, hazardSpawns.Count)];
    }
}
