﻿using System.Collections;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public Vector3 p1Spawn, p2Spawn, p3Spawn, p4Spawn;
    public GameObject jimmyObject;
    private GlobalInfo global;
    private Quaternion jimmyRotation = Quaternion.Euler(new Vector3(35.0f, 0, 0));

    void Start()
    {
        AudioController.PlayMusic("MainThemeIntro");

        global = GameObject.FindGameObjectWithTag("Global").GetComponent<GlobalInfo>();

        if (global.Player1Ready)
        {
            var jimmy1 = (GameObject)Instantiate(jimmyObject, p1Spawn, jimmyRotation);
            jimmy1.GetComponent<JimmyBehavior>().SetPlayerID(1);
        }
        if (global.Player2Ready)
        {
            var jimmy2 = (GameObject)Instantiate(jimmyObject, p2Spawn, jimmyRotation);
            jimmy2.GetComponent<JimmyBehavior>().SetPlayerID(2);
        }
        if (global.Player3Ready)
        {
            var jimmy3 = (GameObject)Instantiate(jimmyObject, p3Spawn, jimmyRotation);
            jimmy3.GetComponent<JimmyBehavior>().SetPlayerID(3);
        }
        if (global.Player4Ready)
        {
            var jimmy4 = (GameObject)Instantiate(jimmyObject, p4Spawn, jimmyRotation);
            jimmy4.GetComponent<JimmyBehavior>().SetPlayerID(4);
        }

        GetComponent<SpawnHazards>().OnReset();

        Physics.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerBoundaries"), LayerMask.NameToLayer("Triggers"));
    }
}