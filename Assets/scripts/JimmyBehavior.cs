﻿using UnityEngine;
using System.Collections;

public class JimmyBehavior : MonoBehaviour 
{
    private int playerID;

    public void SetPlayerID(int id)
    {
        playerID = id;
        GetComponent<Animator>().SetInteger("Player_ID", playerID);
        GetComponent<SpriteRenderer>().enabled = true;
    }
	
	void Update () {
	
	}
}
