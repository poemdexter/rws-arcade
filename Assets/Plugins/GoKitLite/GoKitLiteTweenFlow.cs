using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// totally optional classes for making tween flows
/// </summary>

namespace Prime31.GoKitLite
{
    public class TweenFlow
    {
        private readonly List<TweenFlowItem> _tweenFlows = new List<TweenFlowItem>();
        private int _completionHandlersWaitingToFire;
        private int _currentlyRunningTweenId;
        private Action _onComplete;


        private void onTweenComplete(Transform trans)
        {
            _completionHandlersWaitingToFire--;

            if (_completionHandlersWaitingToFire == 0 && _tweenFlows.Count == 0 && _onComplete != null)
                _onComplete();
        }


        public TweenFlow add(float startTime, Func<GoKitLite.Tween> actionTween)
        {
            _tweenFlows.Add(new TweenFlowItem(startTime, actionTween));
            return this;
        }


        public TweenFlow setCompletionHandler(Action onComplete)
        {
            _onComplete = onComplete;
            return this;
        }


        public IEnumerator start()
        {
            // sort our list so we can iterate backwards and remove items as necessary
            _tweenFlows.Sort();

            // state for the flow
            float elapsedTime = 0f;
            bool running = true;

            while (running)
            {
                elapsedTime += Time.deltaTime;

                // loop backwards so we can remove items as we run them and break the loop when we get past any flow items set to run
                for (int i = _tweenFlows.Count - 1; i >= 0; i--)
                {
                    if (elapsedTime >= _tweenFlows[i].startTime)
                    {
                        TweenFlowItem flowItem = _tweenFlows[i];
                        _tweenFlows.RemoveAt(i);
                        _currentlyRunningTweenId = flowItem.actionTween().setCompletionHandler(onTweenComplete).getId();
                        _completionHandlersWaitingToFire++;
                    }
                    else
                    {
                        break;
                    }
                }

                yield return null;
            }
        }


        public void stop(bool bringCurrentlyRunningTweenToCompletion)
        {
            _tweenFlows.Clear();
            GoKitLite.instance.stopTween(_currentlyRunningTweenId, bringCurrentlyRunningTweenToCompletion);
        }

        /// <summary>
        ///     used internally as a wrapper to handle TweenFlows
        /// </summary>
        internal struct TweenFlowItem : IComparable
        {
            internal Func<GoKitLite.Tween> actionTween;
            internal float startTime;


            public TweenFlowItem(float startTime, Func<GoKitLite.Tween> actionTween)
            {
                this.actionTween = actionTween;
                this.startTime = startTime;
            }


            public int CompareTo(object obj)
            {
                return ((TweenFlowItem) obj).startTime.CompareTo(startTime);
            }
        }
    }
}